pragma solidity ^0.4.22;
import "github.com/Arachnid/solidity-stringutils/strings.sol";

contract myContract
{
   
    using strings for *;
    //=============================
    struct Certifier
    {
        string name;
        bool ifCertifier;
        bytes32[] certifications_given;
    }
   
    mapping(address => mapping(string => uint)) jobsPerCertiArr;
    mapping(address => mapping(string => uint)) givenCertiArr;
       
   
    mapping(address => Certifier) public certifierArr;

    modifier onlyCertifier() {
        require(isCertifier(msg.sender));
        _;
    }
   
    function isCertifier(address account) public view returns (bool) {
        return certifierArr[account].ifCertifier;
    }
   
    function addCertifier(string name, string certifications_given)  {

        Certifier oCertifier;
        oCertifier.name = name;
        oCertifier.ifCertifier = true;
       // oCertifier.certifications_given = certifications_given;
        certifierArr[msg.sender] = oCertifier;

        var s = certifications_given.toSlice();
        var delim = ",".toSlice();
        var parts = new bytes32[](s.count(delim) + 1);
   
        for(uint i = 0; i < parts.length; i++) {
           parts[i] = stringToBytes32(s.split(delim).toString());
        }
       
        oCertifier.certifications_given = parts;
       
        for(uint j = 0; i < parts.length; j++) {
            jobsPerCertiArr[msg.sender][bytes32ToString(parts[j])] = 0;
            givenCertiArr[msg.sender][bytes32ToString(parts[j])] = 0;
        }
    }

       
    //===============================
   
    struct Company
    {
        string name;
        bool ifCompany;
    }
   
    mapping(address => Company) public companyArr;
   
    modifier onlyCompany() {
        require(isCompany(msg.sender));
        _;
    }

    function isCompany(address account) public view returns (bool) {
        return companyArr[account].ifCompany;
    }
   
    function addCompany(string name)  {
        Company oCompany;
        oCompany.name = name;
        oCompany.ifCompany = true;
        companyArr[msg.sender] = oCompany;
        numberOfJobs[msg.sender] = 0;
    }
   
   
     //====================================
     
    struct JobSeeker
    {
        string name;
        uint age;
        bool ifJobSeeker;
    }
   
    mapping(address => JobSeeker) public jobSeekerArr;
   
    modifier onlyJobSeeker() {
        require(isJobSeeker(msg.sender));
        _;
    }
   
    function isJobSeeker(address account) public view returns (bool) {
        return jobSeekerArr[account].ifJobSeeker;
    }
   
    function addJobSeeker(string name, uint age)  {
        JobSeeker oJobSeeker;
        oJobSeeker.name = name;
        oJobSeeker.age = age;
        jobSeekerArr[msg.sender] = oJobSeeker;
    }
   
//===================================================


    struct Certificate
    {
        address certifier_addr;
        string certification_name;
        string grades;
        string expiryDate;
    }
   
    struct JobProfile {
        string company;
        string job_name;
        uint vacancies;
        uint salary;
        bytes32[] certifications_req;
        bool active;
    }
   

    //==============================================
    mapping(address => mapping(uint => Certificate)) public CertificateDataArr;
    mapping(address => uint) public numberOfCertificates;
    mapping(address => mapping(uint => JobProfile)) public JobProfileDataArr;
    mapping(address => mapping(uint =>  mapping(address => string))) public ApplicationsArr;
    mapping(address => uint) public numberOfJobs;
   
    function jobSeekerDetails(address jobSeeker_addr) public returns(string, uint, bytes32[]) {
        uint totalCertificates = numberOfCertificates[jobSeeker_addr];
        bytes32[] certifications;
        for(uint i = 0; i < totalCertificates; i++) {
            certifications[i] = stringToBytes32(CertificateDataArr[jobSeeker_addr][i].certification_name);
        }
        return (jobSeekerArr[jobSeeker_addr].name, jobSeekerArr[jobSeeker_addr].age, certifications);
    }
   
    function create_vacancies(string job_name, uint vacancies, uint salary, bytes32[] certifications_req, bool active) public onlyCompany{
       
        JobProfile oJobProfile;
   
        oJobProfile.company = companyArr[msg.sender].name ;
        oJobProfile.job_name = job_name;
        oJobProfile.vacancies = vacancies;
        oJobProfile.salary = salary;
        oJobProfile.certifications_req = certifications_req;
        oJobProfile.active = active;
       
        JobProfileDataArr[msg.sender][numberOfJobs[msg.sender]] = oJobProfile;
        numberOfJobs[msg.sender] = numberOfJobs[msg.sender] + 1;
    }
   
    function edit_vacancies(uint ID, string job_name, uint vacancies, uint salary, bytes32[] certifications_req, bool active) public onlyCompany {
        JobProfileDataArr[msg.sender][ID].job_name = job_name;
        JobProfileDataArr[msg.sender][ID].vacancies = vacancies;
        JobProfileDataArr[msg.sender][ID].salary = salary;
        JobProfileDataArr[msg.sender][ID].certifications_req = certifications_req;
        JobProfileDataArr[msg.sender][ID].active = active;
       
    }
   
    function display_job(address company_addr, uint ID) public returns (string, string, uint, uint, bytes32[], bool){  
        JobProfile oJobProfile = JobProfileDataArr[company_addr][ID];
        return (oJobProfile.company, oJobProfile.job_name, oJobProfile.vacancies, oJobProfile.salary, oJobProfile.certifications_req, oJobProfile.active);
    }
   
    function getNumberOfJobs(address company_addr) public  returns (uint) {
        return numberOfJobs[company_addr];
    }
   
     function display_certificates(address certifier_addr) returns (string) {
       string certificates;
       var s = certificates.toSlice();
       for(uint i = 0; i< certifierArr[certifier_addr].certifications_given.length; i++){
          var r = bytes32ToString(certifierArr[certifier_addr].certifications_given[i]).toSlice();
            s.concat(r);
       }
       return s.toString();
   }
   
    function assign_certificate(address jobSeeker, string certification_name, string grades, string expiryDate) public onlyCertifier{
       
        Certificate oCertificate;
        oCertificate.certifier_addr = msg.sender;

        oCertificate.certification_name = certification_name;
        oCertificate.grades = grades;
        oCertificate.expiryDate = expiryDate;
       
        CertificateDataArr[jobSeeker][numberOfCertificates[jobSeeker]] = oCertificate;
       
        numberOfCertificates[jobSeeker] = numberOfCertificates[jobSeeker] + 1;
       
        givenCertiArr[msg.sender][certification_name] = givenCertiArr[msg.sender][certification_name] + 1;
       
    }
   
    function isStringEqual(string a, string b) public returns (bool) {
        if(keccak256(abi.encodePacked(a)) == keccak256(abi.encodePacked(b)))
                return true;
        return false;
    }
   
    function validate_certificate(address jobSeeker_addr, string certification_req_name) public returns (bool){
        uint totalCertificates = numberOfCertificates[jobSeeker_addr];
        for(uint i = 0; i < totalCertificates; i++) {
            string certification_name = CertificateDataArr[jobSeeker_addr][i].certification_name;
            if(isStringEqual(certification_name, certification_req_name))
                return true;
        }
        return false;
    }
   
    function bytes32ToString(bytes32 x) constant returns (string) {
        bytes memory bytesString = new bytes(32);
        uint charCount = 0;
        for (uint j = 0; j < 32; j++) {
            byte char = byte(bytes32(uint(x) * 2 ** (8 * j)));
            if (char != 0) {
                bytesString[charCount] = char;
                charCount++;
            }
        }
        bytes memory bytesStringTrimmed = new bytes(charCount);
        for (j = 0; j < charCount; j++) {
            bytesStringTrimmed[j] = bytesString[j];
        }
        return string(bytesStringTrimmed);
    }
   
    function stringToBytes32(string memory source) returns (bytes32 result) {
        bytes memory tempEmptyStringTest = bytes(source);
        if (tempEmptyStringTest.length == 0) {
            return 0x0;
    }

    assembly {
        result := mload(add(source, 32))
    }
}
   
    function ifJobSeekerAllowed(address jobSeeker_addr, bytes32[] certifications_req) public returns (bool){
        for(uint i = 0; i < certifications_req.length; i++) {
            string memory converted = bytes32ToString(certifications_req[i]);
            if(!validate_certificate(jobSeeker_addr, converted ) )
                return false;
        }
        return true;
    }

    function hire_jobseeker(address jobSeeker_addr, uint ID) public onlyCompany returns (bool, string) {
        if (JobProfileDataArr[msg.sender][ID].vacancies == 0)
            return (false, "No vacancies");
       
        if (!ifJobSeekerAllowed(jobSeeker_addr, JobProfileDataArr[msg.sender][ID].certifications_req))
            return (false, "Certification criteria failed");
       
        JobProfileDataArr[msg.sender][ID].vacancies = JobProfileDataArr[msg.sender][ID].vacancies - 1;
        ApplicationsArr[msg.sender][ID][jobSeeker_addr] = "A";
       
        uint totalCertificates = numberOfCertificates[jobSeeker_addr];
        address certifier_addr;
        string certification_name;
        for(uint i = 0; i < totalCertificates; i++) {
            certifier_addr = CertificateDataArr[jobSeeker_addr][i].certifier_addr;
            certification_name = CertificateDataArr[jobSeeker_addr][i].certification_name;
            jobsPerCertiArr[certifier_addr][certification_name] = jobsPerCertiArr[certifier_addr][certification_name] + 1;

        }
        //pay tokens to certifiers
    }
   
    function getCertificationStats(address certifier_addr, string certification_name) public returns (uint, uint) {
        return (jobsPerCertiArr[certifier_addr][certification_name] ,givenCertiArr[certifier_addr][certification_name]);
    }
   
    function apply_for_job(address company_addr, uint ID) public onlyJobSeeker returns (bool, string) {
       if (!ifJobSeekerAllowed(msg.sender, JobProfileDataArr[company_addr][ID].certifications_req))
            return (false, "Certification criteria failed");
           
        if (isStringEqual(ApplicationsArr[company_addr][ID][msg.sender], "R" ))
            return (false, "You have already been rejected by the company");
           
        if (isStringEqual(ApplicationsArr[company_addr][ID][msg.sender], "P"))
            return (false, "Your application is pending");
           
        if (isStringEqual(ApplicationsArr[company_addr][ID][msg.sender], "A"))
            return (false, "You have been already selected!");
           
       ApplicationsArr[company_addr][ID][msg.sender] = "P";
       return (true, "Applied for Job successfully!");
       
    }
   
    function reject_jobseeker(address jobSeeker_addr, uint ID) public onlyCompany{
        ApplicationsArr[msg.sender][ID][jobSeeker_addr] = "R";
    }
   

}